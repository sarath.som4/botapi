package com.bmw.botapi.repository;

import com.bmw.botapi.model.IntentType;

public interface IBotResponseRepository {

    String getResponseTemplate(String attribute, String entity, IntentType intentType);

    String getResponseTemplate(String entity, IntentType intentType);

    String getResponseTemplate(IntentType intentType);

    boolean createResponseEntry(String attribute, String entity, IntentType intentType, String template);
}
