package com.bmw.botapi.repository;

import com.bmw.botapi.infra.Config;
import com.bmw.botapi.model.IntentType;
import com.bmw.botapi.util.StringUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

@Repository
public class SqliteBotResponseRepo extends AbstractSqliteRepository implements IBotResponseRepository {

    private Logger logger = LogManager.getLogger(getClass());

    private static final String CREATE_BOT_RESPONSE_TEMPLATE =
            "CREATE TABLE IF NOT EXISTS bot_response_template (id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " attribute TEXT, entity TEXT, intent TEXT, template TEXT); ";

    private static final String CREATE_INDEX_ON_ATTRIBUTE =
            "CREATE INDEX IF NOT EXISTS bot_response_template_attribute_IDX ON bot_response_template (\"attribute\");";

    private static final String CREATE_INDEX_ON_ENTITY =
            "CREATE INDEX IF NOT EXISTS bot_response_template_entity_IDX ON bot_response_template (entity);";

    private static final String CREATE_INDEX_ON_INTENT =
            "CREATE INDEX IF NOT EXISTS bot_response_template_intent_IDX ON bot_response_template (intent);";

    private static final String INSERT_BOT_RESPONSE_TEMPLATE =
            "INSERT INTO bot_response_template (attribute, entity, intent, template) VALUES (?, ?, ?, ?)";

    private static final String READ_BOT_RESPONSE_TEMPLATE =
            "SELECT template FROM bot_response_template WHERE lower(attribute) = lower(?) AND lower(entity) = lower(?) AND lower(intent) = lower(?)";

    private static final String READ_COUNT_OF_RECORDS =
            "SELECT COUNT(*) AS no_rec FROM bot_response_template";

    @Autowired
    private Config config;

    @PostConstruct
    protected void postConstruct() throws SQLException, IOException {
        createTableAndIndex();
        loadDataOnFirstStartup();
    }

    private void createTableAndIndex() throws SQLException {
        Connection con = getConnection();

        //Create table
        PreparedStatement pst = con.prepareStatement(CREATE_BOT_RESPONSE_TEMPLATE);
        pst.execute();
        pst.close();

        //create attribute index
        pst = con.prepareStatement(CREATE_INDEX_ON_ATTRIBUTE);
        pst.execute();
        pst.close();

        //create entity index
        pst = con.prepareStatement(CREATE_INDEX_ON_ENTITY);
        pst.execute();
        pst.close();

        //create intent index
        pst = con.prepareStatement(CREATE_INDEX_ON_INTENT);
        pst.execute();
        pst.close();

        con.close();
    }

    private void loadDataOnFirstStartup() throws IOException, SQLException {
        Integer noOfRecords = getNumberOfRecords();

        if (null != noOfRecords && noOfRecords > 0) {
            logger.info("Initial Data load to DB - false, noOfRecords: " + noOfRecords);
            return;
        }

        logger.info("Initial Data load to DB - true");

        loadDataToDB();

        loadFAQDataToDB();

    }

    public Integer getNumberOfRecords() throws SQLException {
        Connection con = getConnection();
        PreparedStatement pst = con.prepareStatement(READ_COUNT_OF_RECORDS);
        ResultSet rs = pst.executeQuery();
        Integer noOfRec = null;
        while (rs.next()) {
            noOfRec = rs.getInt("no_rec");
            break;
        }
        pst.close();
        con.close();
        return noOfRec;
    }

    private void loadDataToDB() throws IOException {
        InputStream botResponseTemplateStream = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("bot_response_template.csv");
        BufferedReader br = new BufferedReader(new InputStreamReader(botResponseTemplateStream));
        br.lines()
                .map(line -> line.split(","))
                .forEach(row -> createResponseEntry(row[0].trim(), row[1].trim(), IntentType.valueOf(row[2].trim()), row[3].trim()));
        br.close();
    }

    private void loadFAQDataToDB() throws IOException {
        InputStream faqJsonStream = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("faq.json");
        BufferedReader br = new BufferedReader(new InputStreamReader(faqJsonStream));
        JsonNode jsonNode = new ObjectMapper().readTree(br);

        //load answers
        Map<Integer, String> answers = new HashMap<>();
        jsonNode.get("answers")
                .elements()
                .forEachRemaining(a -> answers.put(a.get("id").asInt(), a.get("answer").textValue().trim()));

        jsonNode.get("questions")
                .elements()
                .forEachRemaining(q -> {
                    JsonNode entityNode = q.get("entity");
                    JsonNode attributeNode = q.get("attribute");
                    JsonNode answerNode = q.get("answer");

                    String entity = null == entityNode ? "" : entityNode.textValue().trim();
                    String attribute = null == attributeNode ? "" : attributeNode.textValue().trim();
                    Integer answerIndex = null == answerNode ? 0 : answerNode.asInt();
                    String answer = 0 == answerIndex ? "" : answers.get(answerIndex);
                    createResponseEntry(attribute, entity, IntentType.FAQ, answer);
                });

        br.close();

    }

    @Override
    public String getResponseTemplate(String attribute, String entity, IntentType intentType) {
        Assert.notNull(intentType, "Intent type is mandatory for getting the response template");
        attribute = StringUtils.clearOutWhiteSpacesBetweenWords(attribute, 1);
        entity = StringUtils.clearOutWhiteSpacesBetweenWords(entity, 1);
        Connection con = null;
        try {
            con = getConnection();
            PreparedStatement pst = con.prepareStatement(READ_BOT_RESPONSE_TEMPLATE);
            pst.setString(1, null == attribute ? "" : attribute.trim());
            pst.setString(2, null == entity ? "" : entity.trim());
            pst.setString(3, intentType.name());
            ResultSet rs = pst.executeQuery();
            String template = null;
            while (rs.next()) {
                template = rs.getString("template");
                break;
            }
            pst.close();
            con.close();
            return template;
        } catch (SQLException e) {
            logger.info("Unable to read bot response entry for attribute: " + attribute + ", entity: "
                    + entity + ", intent: " + intentType.name(), e);
            closeSilently(con);
            return null;
        }
    }

    @Override
    public String getResponseTemplate(String entity, IntentType intentType) {
        return getResponseTemplate(null, entity, intentType);
    }

    @Override
    public String getResponseTemplate(IntentType intentType) {
        return getResponseTemplate(null, null, intentType);
    }

    @Override
    public boolean createResponseEntry(String attribute, String entity, IntentType intentType, String template) {
        Connection con = null;
        try {
            con = getConnection();
            PreparedStatement pst = con.prepareStatement(INSERT_BOT_RESPONSE_TEMPLATE);
            pst.setString(1, attribute);
            pst.setString(2, entity);
            pst.setString(3, intentType.name());
            pst.setString(4, template);
            int rowCount = pst.executeUpdate();
            pst.close();
            con.close();
            return rowCount != 0;
        } catch (SQLException e) {
            logger.info("Bot response entry creation failed", e);
            closeSilently(con);
            return false;
        }
    }
}
