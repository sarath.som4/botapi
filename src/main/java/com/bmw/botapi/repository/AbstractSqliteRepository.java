package com.bmw.botapi.repository;

import com.bmw.botapi.infra.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class AbstractSqliteRepository {

    Logger logger = LogManager.getLogger(getClass());

    @Autowired
    protected Config config;

    protected Connection getConnection() throws SQLException {
        return DriverManager.getConnection(config.getDbConnectionUri());
    }

    protected void closeSilently(Connection con) {
        if (null != con) {
            try {
                con.close();
            } catch (SQLException e1) {
                logger.info("Connection closure failed", e1);
            }
        }
    }
}
