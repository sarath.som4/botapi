package com.bmw.botapi.util;

import java.util.stream.Stream;

public class StringUtils {
    public static String clearOutWhiteSpacesBetweenWords(String txt, int noOfWhiteSpacesToKeep) {
        if (null == txt || "".equals(txt)) {
            return txt;
        }
        return Stream.of(txt.split(" "))
                .map(word -> word.trim())
                .filter(word -> !"".equals(word))
                .reduce((w1, w2) -> w1 + " " + w2)
                .orElse("");
    }
}
