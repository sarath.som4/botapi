package com.bmw.botapi.infra;

import com.bmw.botapi.exception.CredentialValidationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;

@Component
public class Config {

    private Logger logger = LogManager.getLogger(getClass());

    @Value("${google.application.credentials}")
    private String googleApplicationCredentials;

    @Value("${google.project.id}")
    private String googleProjectId;

    @Value("${ba.db.connection.uri}")
    private String dbConnectionUri;

    @Value("${ba.log.location}")
    private String logLocation;

    @Value("${ba.log.trigger.policy.time}")
    private String logTriggerPolicyTime;

    @Value("${ba.log.trigger.policy.size}")
    private String logTriggerPolicySize;

    @Value("${ba.log.roll.over.strategy}")
    private String logRollOverStrategy;

    @Value("${ba.db.reset}")
    private Boolean isDBReset;

    @PostConstruct
    protected void init() {
        if (null == googleApplicationCredentials || "".equals(googleApplicationCredentials)) {
            throw new CredentialValidationException("google.application.credentials is not a valid entry");
        } else if (null == googleProjectId || "".equals(googleProjectId)) {
            throw new CredentialValidationException("google.project.id is not a valid entry");
        } else if (null == dbConnectionUri || "".equals(dbConnectionUri)) {
            throw new CredentialValidationException("ba.db.connection.uri is not a valid entry");
        }

        setupEnv();
    }

    private void setupEnv() {
        if(isDBReset){
            String dbPath = dbConnectionUri.substring(12);
            File db = new File(dbPath);
            if(db.exists()){
                logger.info("Deleting Existing DB: " + dbPath + ", Status: " + db.delete());
            }
        }
    }

    public String getGoogleApplicationCredentials() {
        return googleApplicationCredentials;
    }

    public String getGoogleProjectId() {
        return googleProjectId;
    }

    public String getDbConnectionUri() {
        return dbConnectionUri;
    }


    public String getLogLocation() {
        return logLocation;
    }

    public String getLogTriggerPolicyTime() {
        return logTriggerPolicyTime;
    }

    public String getLogTriggerPolicySize() {
        return logTriggerPolicySize;
    }

    public String getLogRollOverStrategy() {
        return logRollOverStrategy;
    }

    @Override
    public String toString() {
        return "Config{" +
                "googleApplicationCredentials='" + googleApplicationCredentials + '\'' +
                ", googleProjectId='" + googleProjectId + '\'' +
                ", dbConnectionUri='" + dbConnectionUri + '\'' +
                ", logLocation='" + logLocation + '\'' +
                ", logTriggerPolicyTime='" + logTriggerPolicyTime + '\'' +
                ", logTriggerPolicySize='" + logTriggerPolicySize + '\'' +
                ", logRollOverStrategy='" + logRollOverStrategy + '\'' +
                '}';
    }
}
