package com.bmw.botapi.exception;

public class CredentialValidationException extends RuntimeException {

    private final String message;

    public CredentialValidationException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
