package com.bmw.botapi.controller;

import com.bmw.botapi.dto.BotRequest;
import com.bmw.botapi.dto.BotResponse;
import com.bmw.botapi.dto.FulfillmentRequest;
import com.bmw.botapi.dto.FulfillmentResponse;
import com.bmw.botapi.service.DialogflowService;
import com.bmw.botapi.service.IntentFulfilmentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class BotAPIController {

    private Logger logger = LogManager.getLogger(BotAPIController.class);

    @Autowired
    private DialogflowService dialogflowService;

    @Autowired
    private IntentFulfilmentService intentFulfilmentService;

    @PostMapping("/message")
    public ResponseEntity<BotResponse> message(@RequestBody BotRequest request) {
        if (null == request.getAnonymousId() || "".equals(request.getAnonymousId())) {
            request.setAnonymousId(UUID.randomUUID().toString());
            logger.info("First request from client : Generating anonymous id for user");
        }
        logger.info("User utterance (" + request.getAnonymousId() + "): " + request.getMessage());
        BotResponse botResponse = dialogflowService.handleUserUtterance(request);
        logger.info("BotResponse (" + botResponse.getAnonymousId() + "): " + botResponse.getMessage());
        return ResponseEntity.ok(botResponse);
    }

    @PostMapping("/fulfill")
    public ResponseEntity<FulfillmentResponse> fulfillIntent(@RequestBody FulfillmentRequest request) {
        logger.info("Fulfill req: " + request);
        FulfillmentResponse fulfillmentResponse = intentFulfilmentService.fulfilIntent(request);
        logger.info("Fulfill resp: " + fulfillmentResponse);
        return ResponseEntity.ok(fulfillmentResponse);
    }
}
