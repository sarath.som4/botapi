package com.bmw.botapi.controller;

import com.bmw.botapi.dto.GeneralResponse;
import com.bmw.botapi.dto.ResponseStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionControllerAdvice {

    private Logger logger = LogManager.getLogger(ExceptionControllerAdvice.class);

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<GeneralResponse> handleHttpMessageNotReadableException(
            HttpMessageNotReadableException e) {
        logger.error(e);
        return ResponseEntity.badRequest()
                .body(new GeneralResponse(new ResponseStatus(4000, "BadRequest")));
    }

}
