package com.bmw.botapi.model;

public enum IntentType {
    Availability,
    TestDrive,
    ModelDetails,
    ConfigureCar,
    ServicesAndWarranty,
    FinancialServices,
    ContactUs,
    FAQ, Default
}