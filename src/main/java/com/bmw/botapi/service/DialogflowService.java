package com.bmw.botapi.service;

import com.bmw.botapi.dto.BotRequest;
import com.bmw.botapi.dto.BotResponse;
import com.bmw.botapi.dto.FulfillmentMessage;
import com.bmw.botapi.dto.ResponseStatus;
import com.bmw.botapi.infra.Config;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.api.gax.rpc.ApiException;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.dialogflow.v2.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class DialogflowService {

    private Logger logger = LogManager.getLogger(DialogflowService.class);

    @Autowired
    private Config config;

    private SessionsClient sessionsClient;

    @PostConstruct
    protected void init() throws IOException {
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream(new File(config.getGoogleApplicationCredentials())));
//        Credentials credentials = GoogleCredentials.getApplicationDefault();
        SessionsSettings sessionsSettings =
                SessionsSettings.newBuilder()
                        .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
                        .build();
        sessionsClient = SessionsClient.create(sessionsSettings);
    }


    public BotResponse handleUserUtterance(BotRequest request) {
        SessionName session = SessionName.of(config.getGoogleProjectId(), request.getAnonymousId());
        QueryInput queryInput = QueryInput.newBuilder()
                .setText(TextInput.newBuilder()
                        .setLanguageCode("en")
                        .setText(request.getMessage())
                        .build())
                .build();
        try {
            DetectIntentResponse response = sessionsClient.detectIntent(session, queryInput);
            return buildBotResponse(request.getAnonymousId(), Optional.of(response));
        } catch (ApiException e) {
            logger.error("Could not get a response from dialogflow. ", e);
            return buildBotResponse(request.getAnonymousId(), Optional.empty());
        }
    }

    private BotResponse buildBotResponse(String anonymousId, Optional<DetectIntentResponse> response) {
        if (response.isPresent()) {
//            logger.info(response.get().getQueryResult().getFulfillmentMessagesList());
            QueryResult queryResult = response.get().getQueryResult();
            BotResponse botResp = BotResponse.builder()
                    .anonymousId(anonymousId)
                    .message(queryResult.getFulfillmentText())
                    .cards(buildLinkOutSuggestionCardFromIntentResponse(queryResult))
                    .status(ResponseStatus.builder()
                            .code(1000)
                            .message("SUCCESS")
                            .build())
                    .build();
            return botResp;
        }

        return BotResponse.builder()
                .message("Sorry I'm not able to communicate with our systems now.")
                .anonymousId(anonymousId)
                .status(ResponseStatus.builder()
                        .code(9000)
                        .message("UNABLE_TO_COMMUNICATE")
                        .build())
                .build();
    }

    private Map<String, Object> buildLinkOutSuggestionCardFromIntentResponse(QueryResult queryResult) {
        List<Intent.Message> messages = queryResult.getFulfillmentMessagesList();
        if (null == messages || messages.isEmpty()) return null;

        Optional<Intent.Message.LinkOutSuggestion> linkOutSuggestion = messages.stream()
                .filter(Intent.Message::hasLinkOutSuggestion)
                .map(message -> message.getLinkOutSuggestion())
                .findFirst();

        if (linkOutSuggestion.isPresent()) {
            return FulfillmentMessage.cardBuilder()
                    .property("linkOutSuggestion", FulfillmentMessage.cardBuilder()
                            .property("destinationName", linkOutSuggestion.get().getDestinationName())
                            .property("uri", linkOutSuggestion.get().getUri())
                            .build())
                    .build();
        }

        return null;
    }
}
