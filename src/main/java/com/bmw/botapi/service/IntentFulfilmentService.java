package com.bmw.botapi.service;

import com.bmw.botapi.dto.FulfillmentMessage;
import com.bmw.botapi.dto.FulfillmentRequest;
import com.bmw.botapi.dto.FulfillmentResponse;
import com.bmw.botapi.dto.QueryResult;
import com.bmw.botapi.model.IntentType;
import com.bmw.botapi.repository.IBotResponseRepository;
import com.bmw.botapi.util.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class IntentFulfilmentService {

    private Logger logger = LogManager.getLogger(getClass());

    @Autowired
    private IBotResponseRepository botResponseRepository;

    public FulfillmentResponse fulfilIntent(FulfillmentRequest request) {
        QueryResult queryResult = request.getQueryResult();
        if (null == queryResult) {
            return new FulfillmentResponse("Sorry we are unable to answer your queries at this moment");
        }

        String action = queryResult.getAction();
        if (null == action) {
            return new FulfillmentResponse("Sorry we are unable to answer your queries at this moment");
        }

        Map<String, Object> parameters = queryResult.getParameters();
        String queryText = queryResult.getQueryText();

        switch (action) {
            case "ModelDetailsAction":
                return fulfilModelDetailsAction(parameters);
            case "FinancialServicesAction":
                return fulfilFinancialServicesAction(parameters);
            case "TestDriveAction":
                return fulfilTestDriveAction(parameters);
            case "ConfigureCarAction":
                return fulfilConfigureCarAction(parameters);
            case "ContactUsAction":
                return fulfilContactUsAction(parameters);
            case "ServicesAndWarrantyAction":
                return fulfilServicesAndWarrantyAction(parameters);
            case "AvailabilityAction":
                return fulfilAvailabilityAction(parameters);
            case "FAQAction":
                return fulfilFAQAction(parameters);
            default:
                return fulfilDefault(parameters);
        }
    }

    private FulfillmentResponse fulfilModelDetailsAction(Map<String, Object> parameters) {
        Pair<Boolean, FulfillmentResponse> validationAndResp = validateParametersAndGenResp(parameters);
        if (!validationAndResp.getX()) {
            return validationAndResp.getY();
        }

        String attribute = (String) parameters.get("attribute");
        String model = (String) parameters.get("model");
        IntentType intentType = IntentType.ModelDetails;

        String template = botResponseRepository.getResponseTemplate(attribute, model, intentType);
        if (null == template) {
            return buildGeneralResponse(intentType);
        }

        return buildSuccessRespWithLinkOutSuggestion(template);
    }

    private FulfillmentResponse fulfilFinancialServicesAction(Map<String, Object> parameters) {
        Pair<Boolean, FulfillmentResponse> validationAndResp = validateParametersAndGenResp(parameters);
        if (!validationAndResp.getX()) {
            return validationAndResp.getY();
        }

        String attribute = (String) parameters.get("attribute");
        String entity = (String) parameters.get("entity");
        IntentType intentType = IntentType.FinancialServices;

        String template = botResponseRepository.getResponseTemplate(attribute, entity, intentType);
        if (null == template) {
            return buildGeneralResponse(intentType);
        }

        return buildSuccessRespWithLinkOutSuggestion(template);
    }

    private FulfillmentResponse fulfilTestDriveAction(Map<String, Object> parameters) {
        Pair<Boolean, FulfillmentResponse> validationAndResp = validateParametersAndGenResp(parameters);
        if (!validationAndResp.getX()) {
            return validationAndResp.getY();
        }

        String model = (String) parameters.get("model");
        String attribute = (String) parameters.get("attribute");
        IntentType intentType = IntentType.TestDrive;

        String template = botResponseRepository.getResponseTemplate(attribute, model, intentType);
        if (null == template) {
            return buildGeneralResponse(intentType);
        }

        return buildSuccessRespWithLinkOutSuggestion(template);
    }

    private FulfillmentResponse fulfilConfigureCarAction(Map<String, Object> parameters) {
        Pair<Boolean, FulfillmentResponse> validationAndResp = validateParametersAndGenResp(parameters);
        if (!validationAndResp.getX()) {
            return validationAndResp.getY();
        }

        String action = (String) parameters.get("action");
        String model = (String) parameters.get("model");
        IntentType intentType = IntentType.ConfigureCar;

        String template = botResponseRepository.getResponseTemplate(action, model, intentType);
        if (null == template) {
            return buildGeneralResponse(intentType);
        }

        return buildSuccessRespWithLinkOutSuggestion(template);
    }

    private FulfillmentResponse fulfilContactUsAction(Map<String, Object> parameters) {
        IntentType intentType = IntentType.ContactUs;

        String template = botResponseRepository.getResponseTemplate(intentType);
        if (null == template) {
            return buildGeneralResponse(intentType);
        }

        return buildSuccessRespWithLinkOutSuggestion(template);
    }

    private FulfillmentResponse fulfilServicesAndWarrantyAction(Map<String, Object> parameters) {
        Pair<Boolean, FulfillmentResponse> validationAndResp = validateParametersAndGenResp(parameters);
        if (!validationAndResp.getX()) {
            return validationAndResp.getY();
        }

        String attribute = (String) parameters.get("attribute");
        String model = (String) parameters.get("model");
        IntentType intentType = IntentType.ServicesAndWarranty;

        String template = botResponseRepository.getResponseTemplate(attribute, model, intentType);
        if (null == template) {
            return buildGeneralResponse(intentType);
        }

        return buildSuccessRespWithLinkOutSuggestion(template);
    }

    private FulfillmentResponse fulfilAvailabilityAction(Map<String, Object> parameters) {
        IntentType intentType = IntentType.Availability;

        String template = botResponseRepository.getResponseTemplate(intentType);
        if (null == template) {
            return buildGeneralResponse(intentType);
        }

        return buildSuccessRespWithLinkOutSuggestion(template);
    }

    private FulfillmentResponse fulfilFAQAction(Map<String, Object> parameters) {
        Pair<Boolean, FulfillmentResponse> validationAndResp = validateParametersAndGenResp(parameters);
        if (!validationAndResp.getX()) {
            return validationAndResp.getY();
        }

        String attribute = (String) parameters.get("attribute");
        String entity = (String) parameters.get("entity");
        IntentType intentType = IntentType.FAQ;

        String answer = botResponseRepository.getResponseTemplate(attribute, entity, intentType);
        if (null == answer) {
            return buildGeneralResponse(intentType);
        }

        return FulfillmentResponse.builder()
                .fulfillmentText(answer)
                .build();
    }

    private FulfillmentResponse fulfilDefault(Map<String, Object> parameters) {
        IntentType intentType = IntentType.Default;

        String template = botResponseRepository.getResponseTemplate(intentType);
        if (null == template) {
            return buildGeneralResponse(intentType);
        }

        return buildSuccessRespWithLinkOutSuggestion(template);
    }

    private FulfillmentResponse dumpResponseBack(Map<String, Object> parameters) {
        String resp = parameters.entrySet()
                .stream()
                .map(entry -> entry.getKey() + "-" + entry.getValue())
                .reduce((s1, s2) -> s1 + "||" + s2)
                .orElse("");
        return FulfillmentResponse.builder()
                .fulfillmentText(resp)
                .fulfillmentMessage(FulfillmentMessage.cardBuilder()
                        .property("linkOutSuggestion", FulfillmentMessage.cardBuilder()
                                .property("destinationName", "Link BMW")
                                .property("uri", "https://google.com")
                                .build())
                        .build())
                .build();
    }

    private Pair<Boolean, FulfillmentResponse> validateParametersAndGenResp(Map<String, Object> parameters) {
        if (null == parameters) {
            logger.info("Fulfilment parameters is null");
            return new Pair(false, FulfillmentResponse.builder()
                    .fulfillmentText("Sorry I'm unable to find the information you need. Please visit ")
                    .fulfillmentMessage(FulfillmentMessage.cardBuilder()
                            .property("linkOutSuggestion", FulfillmentMessage.cardBuilder()
                                    .property("destinationName", "Home Page")
                                    .property("uri", "https://www.bmw.in/en/index.html")
                                    .build())
                            .build())
                    .build());
        }

        return new Pair<>(true, null);
    }

    private FulfillmentResponse buildGeneralResponse(IntentType intentType) {
        return FulfillmentResponse.builder()
                .fulfillmentText("Sorry I'm unable to find the information you need. Please visit ")
                .fulfillmentMessage(FulfillmentMessage.cardBuilder()
                        .property("linkOutSuggestion", FulfillmentMessage.cardBuilder()
                                .property("destinationName", "Home Page")
                                .property("uri", "https://www.bmw.in/en/index.html")
                                .build())
                        .build())
                .build();
    }

    private FulfillmentResponse buildSuccessRespWithLinkOutSuggestion(String template) {
        return FulfillmentResponse.builder()
                .fulfillmentText("Thank you, for your query. Please visit ")
                .fulfillmentMessage(FulfillmentMessage.cardBuilder()
                        .property("linkOutSuggestion", FulfillmentMessage.cardBuilder()
                                .property("destinationName", "Link")
                                .property("uri", template)
                                .build())
                        .build())
                .build();
    }

}
