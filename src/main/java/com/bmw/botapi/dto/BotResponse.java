package com.bmw.botapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BotResponse {

    private String message;
    private String anonymousId;
    private Map<String, Object> cards;

    private ResponseStatus status;

    public BotResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public String getAnonymousId() {
        return anonymousId;
    }

    public void setAnonymousId(String anonymousId) {
        this.anonymousId = anonymousId;
    }

    public Map<String, Object> getCards() {
        return cards;
    }

    public void setCards(Map<String, Object> cards) {
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "BotResponse{" +
                "message='" + message + '\'' +
                ", anonymousId='" + anonymousId + '\'' +
                ", cards=" + cards +
                ", status=" + status +
                '}';
    }

    public static BotResponseBuilder builder() {
        return new BotResponseBuilder();
    }

    public static final class BotResponseBuilder {
        private String message;
        private String anonymousId;
        private Map<String, Object> cards;
        private ResponseStatus status;

        private BotResponseBuilder() {
        }

        public BotResponseBuilder message(String message) {
            this.message = message;
            return this;
        }

        public BotResponseBuilder anonymousId(String anonymousId) {
            this.anonymousId = anonymousId;
            return this;
        }

        public BotResponseBuilder cards(Map<String, Object> cards) {
            this.cards = cards;
            return this;
        }

        public BotResponseBuilder status(ResponseStatus status) {
            this.status = status;
            return this;
        }

        public BotResponse build() {
            BotResponse botResponse = new BotResponse();
            botResponse.setMessage(message);
            botResponse.setAnonymousId(anonymousId);
            botResponse.setCards(cards);
            botResponse.setStatus(status);
            return botResponse;
        }
    }
}