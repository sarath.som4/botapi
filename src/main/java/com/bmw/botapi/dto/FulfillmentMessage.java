package com.bmw.botapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FulfillmentMessage {

    public static CardBuilder cardBuilder() {
        return new CardBuilder();
    }

    public static final class CardBuilder {
        private Map<String, Object> card;

        private CardBuilder() {
        }

        public CardBuilder property(Map<String, Object> card) {
            this.card = card;
            return this;
        }

        public CardBuilder property(String key, Object value) {
            if(null == card) card = new HashMap<>();
            card.put(key, value);
            return this;
        }

        public Map<String, Object> build() {
            return card;
        }
    }
}
