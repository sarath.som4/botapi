package com.bmw.botapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FollowupEventInput {

  private String name;
  private String languageCode;
  private Map<String, String> parameters;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLanguageCode() {
    return languageCode;
  }

  public void setLanguageCode(String languageCode) {
    this.languageCode = languageCode;
  }

  public Map<String, String> getParameters() {
    return parameters;
  }

  public void setParameters(Map<String, String> parameters) {
    this.parameters = parameters;
  }

  @Override
  public String toString() {
    return "FollowupEventInput{" +
        "name='" + name + '\'' +
        ", languageCode='" + languageCode + '\'' +
        ", parameters=" + parameters +
        '}';
  }
}
