package com.bmw.botapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FulfillmentResponse {

  private String fulfillmentText;

  private List<OutputContext> outputContexts;

  private FollowupEventInput followupEventInput;

  private Map<String, Object> payload;

  private List<Map<String, Object>> fulfillmentMessages;

  public FulfillmentResponse(String fulfillmentText) {
    this.fulfillmentText = fulfillmentText;
  }

  public FulfillmentResponse() {}

  public String getFulfillmentText() {
    return fulfillmentText;
  }

  public void setFulfillmentText(String fulfillmentText) {
    this.fulfillmentText = fulfillmentText;
  }

  public List<OutputContext> getOutputContexts() {
    return outputContexts;
  }

  public void setOutputContexts(List<OutputContext> outputContexts) {
    this.outputContexts = outputContexts;
  }

  public FollowupEventInput getFollowupEventInput() {
    return followupEventInput;
  }

  public void setFollowupEventInput(FollowupEventInput followupEventInput) {
    this.followupEventInput = followupEventInput;
  }

  public Map<String, Object> getPayload() {
    return payload;
  }

  public void setPayload(Map<String, Object> payload) {
    this.payload = payload;
  }

  public List<Map<String, Object>> getFulfillmentMessages() {
    return fulfillmentMessages;
  }

  public void setFulfillmentMessages(List<Map<String, Object>> fulfillmentMessages) {
    this.fulfillmentMessages = fulfillmentMessages;
  }

  @Override
  public String toString() {
    return "FulfillmentResponse{" +
            "fulfillmentText='" + fulfillmentText + '\'' +
            ", outputContexts=" + outputContexts +
            ", followupEventInput=" + followupEventInput +
            ", payload='" + payload + '\'' +
            ", fulfillmentMessages=" + fulfillmentMessages +
            '}';
  }

  public static FulfillmentResponseBuilder builder() {
    return new FulfillmentResponseBuilder();
  }

  public static final class FulfillmentResponseBuilder {
    private String fulfillmentText;
    private List<OutputContext> outputContexts;
    private FollowupEventInput followupEventInput;
    private Map<String, Object> payload;
    private List<Map<String, Object>> fulfillmentMessages;

    private FulfillmentResponseBuilder() {
    }

    public FulfillmentResponseBuilder fulfillmentText(String fulfillmentText) {
      this.fulfillmentText = fulfillmentText;
      return this;
    }

    public FulfillmentResponseBuilder outputContexts(List<OutputContext> outputContexts) {
      this.outputContexts = outputContexts;
      return this;
    }

    public FulfillmentResponseBuilder followupEventInput(FollowupEventInput followupEventInput) {
      this.followupEventInput = followupEventInput;
      return this;
    }

    public FulfillmentResponseBuilder payload(Map<String, Object> payload) {
      this.payload = payload;
      return this;
    }

    public FulfillmentResponseBuilder fulfillmentMessages(List<Map<String, Object>> fulfillmentMessages) {
      this.fulfillmentMessages = fulfillmentMessages;
      return this;
    }

    public FulfillmentResponseBuilder fulfillmentMessage(Map<String, Object> fulfillmentMessage) {
      if(null == fulfillmentMessages) fulfillmentMessages = new ArrayList<>();
      fulfillmentMessages.add(fulfillmentMessage);
      return this;
    }

    public FulfillmentResponse build() {
      FulfillmentResponse fulfillmentResponse = new FulfillmentResponse();
      fulfillmentResponse.setFulfillmentText(fulfillmentText);
      fulfillmentResponse.setOutputContexts(outputContexts);
      fulfillmentResponse.setFollowupEventInput(followupEventInput);
      fulfillmentResponse.setPayload(payload);
      fulfillmentResponse.setFulfillmentMessages(fulfillmentMessages);
      return fulfillmentResponse;
    }
  }
}
