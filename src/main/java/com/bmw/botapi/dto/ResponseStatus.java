package com.bmw.botapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseStatus {

    private Integer code;
    private String message;

    public ResponseStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override public String toString() {
        return "BotResponseStatus{" + "code=" + code + ", message='" + message + '\'' + '}';
    }

    public static ResponseStatusBuilder builder() {
        return new ResponseStatusBuilder();
    }


    public static final class ResponseStatusBuilder {
        private Integer code;
        private String message;

        private ResponseStatusBuilder() {
        }

        public ResponseStatusBuilder code(Integer code) {
            this.code = code;
            return this;
        }

        public ResponseStatusBuilder message(String message) {
            this.message = message;
            return this;
        }

        public ResponseStatus build() {
            return new ResponseStatus(code, message);
        }
    }
}
