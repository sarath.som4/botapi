package com.bmw.botapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BotRequest {

    private String message;
    private String anonymousId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAnonymousId() {
        return anonymousId;
    }

    public void setAnonymousId(String anonymousId) {
        this.anonymousId = anonymousId;
    }

    @Override
    public String toString() {
        return "BotRequest{" +
                "message='" + message + '\'' +
                ", anonymousId='" + anonymousId + '\'' +
                '}';
    }
}
