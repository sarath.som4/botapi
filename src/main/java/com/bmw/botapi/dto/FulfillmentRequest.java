package com.bmw.botapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FulfillmentRequest {

  private String responseId;
  private QueryResult queryResult;
  private String session;

  public String getResponseId() {
    return responseId;
  }

  public void setResponseId(String responseId) {
    this.responseId = responseId;
  }

  public QueryResult getQueryResult() {
    return queryResult;
  }

  public void setQueryResult(QueryResult queryResult) {
    this.queryResult = queryResult;
  }

  public String getSession() {
    return session;
  }

  public void setSession(String session) {
    this.session = session;
  }

  @Override
  public String toString() {
    return "FulfillmentRequest{" +
        "responseId='" + responseId + '\'' +
        ", queryResult=" + queryResult +
        ", session='" + session + '\'' +
        '}';
  }

}

/**
 Sample payload 
 
{
  "responseId": "7bd0fe27-6cec-44d3-9a86-611130654aea",
  "queryResult": {
    "queryText": "sadasd",
    "action": "GetPrice",
    "parameters": {
      "productName": "sadasd"
    },
    "allRequiredParamsPresent": true,
    "fulfillmentMessages": [{
      "text": {
        "text": [""]
      }
    }],
    "intent": {
      "name": "projects/merck-bot/agent/intents/a0d5ebe0-1bd3-4e86-b33a-f79712ac6840",
      "displayName": "Product Price"
    },
    "intentDetectionConfidence": 1.0,
    "diagnosticInfo": {
    },
    "languageCode": "en"
  },
  "originalDetectIntentRequest": {
    "payload": {
    }
  },
  "session": "projects/merck-bot/agent/sessions/4789ed9a-c77e-11e3-65ab-a4109dc7dfc3"
}
  
 */
  
