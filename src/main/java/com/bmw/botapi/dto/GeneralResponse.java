package com.bmw.botapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GeneralResponse {

    protected ResponseStatus status;

    public GeneralResponse(ResponseStatus status) {
        this.status = status;
    }

    public GeneralResponse() {
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public ResponseStatus getStatus() {
        return status;
    }
}
