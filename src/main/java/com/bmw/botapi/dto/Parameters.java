package com.bmw.botapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Parameters {
  private String productName;
  private Integer productQuantity;
  private List<String> propertyNames;

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public Integer getProductQuantity() {
    return productQuantity;
  }

  public void setProductQuantity(Integer productQuantity) {
    this.productQuantity = productQuantity;
  }

  public List<String> getPropertyNames() {
    return propertyNames;
  }

  public void setPropertyNames(List<String> propertyNames) {
    this.propertyNames = propertyNames;
  }

  @Override
  public String toString() {
    return "Parameters [productName=" + productName + ", productQuantity=" + productQuantity
        + ", propertyNames=" + propertyNames + "]";
  }


}